# MobiDevTest application

1) clone repo

2) install dependencies
`npm i`

development mode
`npm start`
in browser `http://0.0.0.0:4000`

production mode
`npm run build-prod`

My updates, if i had more time:
1) Add all incoming data validation
2) Add bootstrap
3) Add markup
4) Update undo/redo system, add size checking
5) Add ability to set canvas size as image size