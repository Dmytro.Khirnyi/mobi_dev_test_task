import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-file-select-component',
    templateUrl: './file-select.html',
})
export class FileSelectComponent {

    @Output()
    onFileSelect: EventEmitter<File> = new EventEmitter<File>();

    /**
     * Method to handle selected file
     * @param $event
     */
    public selectFile($event: any): void {
        const file: File = $event.target.files[0];
        this.onFileSelect.emit(file);
    }
}
