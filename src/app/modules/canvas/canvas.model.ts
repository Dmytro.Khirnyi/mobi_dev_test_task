export interface CanvasConfig {
    width: number;
    height: number;
}

export interface DrawEvent {
    currX: number;
    currY: number;
    prevX: number;
    prevY: number;
    strokeStyle: string;
    lineWidth: number;
    flag?: boolean;
    dot_flag?: boolean;
}
