import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {CanvasComponent} from './canvas.component';
import {CanvasControlsComponent} from './controls/canvas-controls.component';
import {FileSelectComponent} from './file-select/file-select.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        CanvasComponent,
        FileSelectComponent,
        CanvasControlsComponent
    ],
    exports: [
        CanvasComponent
    ]
})
export class CanvasModule {}