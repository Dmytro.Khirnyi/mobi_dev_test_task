import {Component, EventEmitter, Input, OnChanges, Output, SimpleChange, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {Color, ControlSettings, ImageFilterOption, Width} from './canvas-controls.model';
import {CanvasConfig} from '../canvas.model';

@Component({
    selector: 'app-canvas-controls-component',
    templateUrl: './canvas-controls.html',
    styles: [require('./canvas-controls.scss').toString()]
})
export class CanvasControlsComponent implements OnChanges {

    @Input()
    colors: Array<Color>;

    @Input()
    width: Array<Width>;

    @Input()
    isShowUndo: Boolean = true;

    @Input()
    isShowRedo: Boolean = true;

    @Input()
    settings: ControlSettings;

    @Input()
    config: CanvasConfig;

    @Output()
    onCanvasClear: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

    @Output()
    onColorChange: EventEmitter<string> = new EventEmitter<string>();

    @Output()
    onWidthChange: EventEmitter<number> = new EventEmitter<number>();

    @Output()
    onCanvasSave: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onUndo: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onRedo: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onResize: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onFilterChange: EventEmitter<any> = new EventEmitter<any>();

    public modelForm: FormGroup;

    public filterOptions: Array<ImageFilterOption> = [
        {
            name: 'Blur',
            values: [
                {
                    title: '1px',
                    value: 'blur(1px)'
                },
                {
                    title: '2px',
                    value: 'blur(2px)'
                },
                {
                    title: '3px',
                    value: 'blur(3px)'
                },
                {
                    title: '4px',
                    value: 'blur(4px)'
                },
                {
                    title: '5px',
                    value: 'blur(5px)'
                },
                {
                    title: '10px',
                    value: 'blur(10px)'
                }
            ]
        },
        {
            name: 'Opacity',
            values: [
                {
                    title: '25%',
                    value: 'opacity(25%)'
                },
                {
                    title: '50%',
                    value: 'opacity(50%)'
                },
                {
                    title: '75%',
                    value: 'opacity(75%)'
                },
            ]
        },
        {
            name: 'Brightness',
            values: [
                {
                    title: '20%',
                    value: 'brightness(0.2)'
                },
                {
                    title: '50%',
                    value: 'brightness(0.5)'
                },
                {
                    title: '75%',
                    value: 'brightness(0.75)'
                },
            ]
        }
    ];

    constructor(private fb: FormBuilder) {
        this.buildForm();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        const settings: SimpleChange = changes['settings'];

        if (settings && settings.currentValue) {
            this.setValue<string>('color', this.settings.stokeColor);
            this.setValue<number>('width', this.settings.strokeWidth);
            this.setValue<number>('imageWidth', this.settings.width);
            this.setValue<number>('imageHeight', this.settings.height);
        }
    }

    public executeTask(event: any, action: string): void {
        switch (action) {
            case 'clear':
                this.onCanvasClear.emit(event);
                break;
            case 'save':
                this.onCanvasSave.emit(event);
                break;
            case 'undo':
                this.onUndo.emit();
                break;
            case 'redo':
                this.onRedo.emit();
                break;
            case 'resize':
                this.resizeCanvas();
                break;
        }
    }

    private resizeCanvas(): void {
        const width: number = +this.modelForm.get('imageWidth').value;
        const height: number = +this.modelForm.get('imageHeight').value;
        if (Number.isInteger(width) && Number.isInteger(height)) {
            if (width > this.config.width || height > this.config.height) {
                alert(`Max width is ${this.config.width}px, height is ${this.config.height}`);
            } else {
                this.onResize.emit({width, height});
            }
        } else {
            alert('Width or height should be integer numbers.');
        }
    }

    private buildForm(): void {
        this.modelForm = this.fb.group({
            'color': [],
            'width': [],
            'imageWidth': [],
            'imageHeight': [],
            'filter': [],
            'filterValue': []
        });

        this.modelForm.get('color')
            .valueChanges
            .subscribe(
                (color: string) => this.onColorChange.emit(color)
            );

        this.modelForm.get('width')
            .valueChanges
            .subscribe(
                (width: number) => this.onWidthChange.emit(width)
            );

        this.modelForm.get('filterValue')
            .valueChanges
            .subscribe(
                (value: string) => this.onFilterChange.emit(value)
            );
    }

    private setValue<Type>(field: string, value: Type): void {
        this.modelForm.get(field).setValue(value);
    }
}
