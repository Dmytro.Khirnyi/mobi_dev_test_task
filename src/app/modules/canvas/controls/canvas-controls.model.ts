export interface Color {
    title: string;
    value: string;
}

export interface Width {
    title: string;
    value: number;
}

export interface ControlSettings {
    strokeWidth: number;
    stokeColor: string;
    width: number;
    height: number;
}

export interface ImageFilterOptionValue {
    title: string;
    value: number | string;
}

export interface ImageFilterOption {
    name: string;
    values: Array<ImageFilterOptionValue>;
}