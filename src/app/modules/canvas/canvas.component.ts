import {Component, ElementRef, Input, OnChanges, OnInit, ViewChild} from '@angular/core';

import {CanvasConfig, DrawEvent} from './canvas.model';
import {Color, ControlSettings, Width} from './controls/canvas-controls.model';
import {StoreService} from '../../core/services/store.service';

@Component({
    selector: 'app-canvas-component',
    templateUrl: './canvas.html',
    styles: [require('./canvas.scss').toString()]
})
export class CanvasComponent implements OnChanges, OnInit {

    @ViewChild('canvas')
    canvas: ElementRef;

    @Input()
    config: CanvasConfig;

    public states: Array<string> = [];
    public stateIndex: Number = 0;

    public drawEvent: DrawEvent = {
        strokeStyle: 'black',
        lineWidth: 2,
        prevX: 0,
        currX: 0,
        prevY: 0,
        currY: 0,
        dot_flag: false
    };

    public colors: Array<Color> = [
        {
            title: 'Black',
            value: 'black'
        },
        {
            title: 'Yellow',
            value: 'yellow'
        },
        {
            title: 'Red',
            value: 'red'
        }
    ];

    public width: Array<Width> = [
        {
            title: 'Two',
            value: 2
        },
        {
            title: 'Three',
            value: 3
        },
        {
            title: 'Four',
            value: 4
        },
        {
            title: 'Five',
            value: 5
        },
    ];

    public controlSettings: ControlSettings;

    constructor(private storeService: StoreService) {
    }

    public ngOnChanges(): void {
        this.controlSettings = {
            strokeWidth: 2,
            stokeColor: 'black',
            width: this.config.width,
            height: this.config.height
        };
    }

    public ngOnInit() {
        this.getContext().fillStyle = 'white';
        this.initCanvas();
        this.checkSaveImage();
    }

    public undoImage(): void {
        (<number>this.stateIndex)--;
        this.setStateImage(+this.stateIndex);
    }

    public redoImage(): void {
        (<number>this.stateIndex)++;
        this.setStateImage(+this.stateIndex);
    }

    private setStateImage(index: number): void {
        if (index > 0) {
            const img: any = new Image();
            img.src = this.states[index - 1];
            img.addEventListener('load', () => {
                this.clearCanvas();
                this.drawImage(img);
            });
        }
    }

    public resizeImage($event: any): void {
        const image: any = new Image();
        image.src = this.getSavedImage();
        image.addEventListener('load', () => {
            this.clearCanvas();
            this.getContext().drawImage(image, 0, 0, $event.width, $event.height);
        });
    }

    public checkSaveImage(): void {
        const imageDataUrl: string = this.storeService.getItem<string>('image');
        if (imageDataUrl) {
            const answer: boolean = confirm('You have saved image. Dou you want to select it?');
            if (answer) {
                const image: any = new Image();
                image.src = imageDataUrl;
                image.addEventListener('load', () => {
                    this.drawImage(image);
                });
            }
        }
    }

    public setFilter(value: string): void {
        (<any>this.getContext()).filter = value;
    }

    public saveImage(): void {
        this.storeService.setItem<string>('image', this.getSavedImage(), true);
    }

    public getSavedImage(): string {
        return this.getCanvasElement().toDataURL('image/png');
    }

    public initCanvas(): void {
        const canvas: HTMLCanvasElement = this.getCanvasElement();

        canvas.addEventListener('mousemove', (e: MouseEvent) => {
            this.findxy('move', e);
        }, false);
        canvas.addEventListener('mousedown', (e: MouseEvent) => {
            this.findxy('down', e);
        }, false);
        canvas.addEventListener('mouseup', (e: MouseEvent) => {
            this.findxy('up', e);
        }, false);
        canvas.addEventListener('mouseout', (e: MouseEvent) => {
            this.findxy('out', e);
        }, false);

    }

    public clearCanvas(clearState?: boolean): void {
        this.getContext().clearRect(0, 0, this.config.width, this.config.height);
        if (clearState) {
            this.stateIndex = 0;
            this.states = [];
        }
    }

    public uploadImage(file: File): void {
        if (file && file instanceof File) {
            const fileReader: FileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.addEventListener('load', (event: any) => {
                const img: HTMLImageElement = new Image();
                img.addEventListener('load', () => {
                    this.initStateStore(event.target.result);
                    this.drawImage(img);
                });
                img.src = event.target.result;
            });
        }
    }

    private initStateStore(dataUrl: string): void {
        this.states = [dataUrl];
        (<number>this.stateIndex) = 1;
    }

    /**
     * Method to draw image in canvas
     * @param {HTMLImageElement} image
     */
    private drawImage(image: HTMLImageElement): void {
        this.getContext().drawImage(image, 0, 0);
    }

    /**
     * Method to change color
     * @param {string} color
     */
    public colorChanged(color: string): void {
        this.drawEvent.strokeStyle = color;
    }

    /**
     * Method to change stroke width
     * @param {number} width
     */
    public widthChanged(width: number): void {
        this.drawEvent.lineWidth = width;
    }

    /**
     * Method to get canvas element
     * @returns {HTMLCanvasElement}
     */
    private getCanvasElement(): HTMLCanvasElement {
        return (<HTMLCanvasElement>this.canvas.nativeElement);
    }

    /**
     * Method to get canvas context
     * @returns {CanvasRenderingContext2D}
     */
    private getContext(): CanvasRenderingContext2D {
        return this.getCanvasElement().getContext('2d');
    }

    /**
     * Method to draw
     * @param {DrawEvent} e
     */
    private draw(e: DrawEvent) {
        const ctx: CanvasRenderingContext2D = this.getContext();
        ctx.beginPath();
        ctx.moveTo(e.prevX, e.prevY);
        ctx.lineTo(e.currX, e.currY);
        ctx.strokeStyle = e.strokeStyle;
        ctx.lineWidth = e.lineWidth;
        ctx.stroke();
        ctx.closePath();
    }

    private findxy(typeAction: string, event: MouseEvent) {
        const canvas: HTMLCanvasElement = this.getCanvasElement();
        const ctx: CanvasRenderingContext2D = this.getContext();
        const e: DrawEvent = this.drawEvent;
        if (typeAction === 'down') {
            e.prevX = e.currX;
            e.prevY = e.currY;
            e.currX = event.clientX - canvas.offsetLeft;
            e.currY = event.clientY - canvas.offsetTop;

            e.flag = true;
            e.dot_flag = true;
            if (e.dot_flag) {
                ctx.beginPath();
                ctx.fillStyle = e.strokeStyle;
                ctx.fillRect(e.currX, e.currY, 2, 2);
                ctx.closePath();
                e.dot_flag = false;
            }
        }
        if (typeAction === 'up' || typeAction === 'out') {
            if (e.flag) {
                this.states.push(this.getSavedImage());
                (<number>this.stateIndex)++;
            }
            e.flag = false;
        }
        if (typeAction === 'move') {
            if (e.flag) {
                e.prevX = e.currX;
                e.prevY = e.currY;
                e.currX = event.clientX - canvas.offsetLeft;
                e.currY = event.clientY - canvas.offsetTop;
                this.draw(this.drawEvent);
            }
        }
    }
}
