import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {StoreService} from './services/store.service';
import {MemoryService} from './services/memory.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [
      StoreService,
      MemoryService
  ]
})
export class CoreModule {}
