export class MemoryService {
  store: Object;

  constructor() {
    this.store = {};
  }

  public setItem(key: string, value: string): void {
    this.store[key] = value;
  }

  public getItem(key: string): string | null {
    if (this.store[key]) {
      return this.store[key];
    }
    return null;
  }
}
