import {Injectable} from '@angular/core';

import {MemoryService} from './memory.service';

@Injectable()
export class StoreService {
  constructor(private memoryService: MemoryService) {
  }

    /**
     * Method to get item by key
     * @param {string} key
     * @returns {Type}
     */
  public getItem<Type>(key: string): Type {
    try {
      return JSON.parse(sessionStorage.getItem(key)) || JSON.parse(localStorage.getItem(key));
    } catch (err) {
      return JSON.parse(this.memoryService.getItem(key));
    }
  }

    /**
     * Method to set item to storage by key
     * @param {string} key
     * @param {Type} valueObj
     * @param {boolean} remember
     */
  public setItem<Type>(key: string, valueObj: Type, remember: boolean = true) {
    try {
      if (remember) {
        localStorage.setItem(key, JSON.stringify(valueObj));
      } else {
        sessionStorage.setItem(key, JSON.stringify(valueObj));
      }
    } catch (err) {
      this.memoryService.setItem(key, JSON.stringify(valueObj));
    }
  }
}
