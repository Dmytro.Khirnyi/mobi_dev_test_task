import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

export const routes: Routes = [
  {
    path: 'home',
    loadChildren: './lazy/home/home.module#HomeModule'
  },
  {
    path: '**',
    redirectTo: '/home'
  }
];
export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes);
