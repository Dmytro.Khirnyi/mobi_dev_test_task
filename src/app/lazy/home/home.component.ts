import {Component} from '@angular/core';

import {CanvasConfig} from '../../modules/canvas/canvas.model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styles: [require('./home.component.scss').toString()]
})
export class HomeComponent {

    public config: CanvasConfig = {
        width: 800,
        height: 500
    };

}
