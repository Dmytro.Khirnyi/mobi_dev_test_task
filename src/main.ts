// Special for IE
import 'es6-shim';
import 'svgxuse';

// Angular dependencies
import 'zone.js';
import 'reflect-metadata';
import 'rxjs';

import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';

declare const ENV_CONFIG;
const {ENV} = ENV_CONFIG;
if (ENV === 'prod') {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
