/*
 * Webpack 2 config file
 */

const webpack = require('webpack');
const path = require('path');

/*
 * Webpack native plugins
 */
// Moves styles into separate css files
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// Generates index.html from template and pastes bundles into it
const HtmlWebpackPlugin = require('html-webpack-plugin');
// Minifies js
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

/*
 * Webpack 3rd-party plugins
 */
// Cleans build folder before build
const CleanWebpackPlugin = require('clean-webpack-plugin');

const srcDir = path.join(__dirname, 'src');
const pubDir = path.join(__dirname, 'public');

module.exports = (env) => {
  const isDev = env && (!!env.dev);
  const isProd = env && (!!env.prod);
  const isStage = env && (!!env.stage);
  let ENV = '';
  if (isDev) {
    ENV = 'dev';
  }
  if (isProd) {
    ENV = 'prod';
  }
  if (isStage) {
    ENV = 'stage';
  }

  const htmlMinifierOptions = {
    caseSensitive: true,
    collapseBooleanAttributes: true,
    collapseWhitespace: true,
    collapseInlineTagWhitespace: true,
    removeComments: true,
    removeRedundantAttributes: true,
    removeScriptTypeAttributes: true,
    removeStyleLinkTypeAttributes: true,
    useShortDoctype: true
  };

  const cssLoader = {
    loader: 'css-loader',
    options: {
      minimize: !isDev,
      autoprefixer: {browsers: ['last 2 versions'], remove: true},
      zindex: false
    }
  };

  let config = {
    context: srcDir,
    entry: {
      main: './main.ts'
    },
    output: {
      path: pubDir,
      publicPath: '',
      filename: 'js/[name].js'
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: ['awesome-typescript-loader', 'angular2-template-loader', 'angular2-router-loader']
        },
        {
          test: /\.(sass|scss)$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              cssLoader,
              'resolve-url-loader',
              'sass-loader'
            ],
            publicPath: '../'
          })
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            cssLoader
          ]
        },
        {
          test: /.*favicon[^.]*\.png$/,
          use: {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            }
          }
        },
        {
          test: /\.(jpe?g|png|gif|svg)(\?[a-z0-9=&.])*$/,
          exclude: /favicon/,
          use: {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]'
            }
          }
        },
        {
          test: /\.(woff2|woff|ttf|eot)(\?[a-z0-9=&.]+)?$/,
          use: {
            loader: 'file-loader',
            options: {
              name: isDev ? '[path][name].[ext]' : 'assets/fonts/[name].[ext]'
            }
          }
        },
        {
          test: /\.html$/,
          use: {
            loader: 'html-loader',
            options: {
              attrs: ['link:href', 'img:src', 'use:xlink:href']
            }
          }
        }
      ]
    },

    // Auto resolving a file extension for import 'filename'
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.json', '.css', '.sass', '.scss', '.html']
    },

    plugins: [
      // Stop build om error
      new webpack.NoEmitOnErrorsPlugin(),

      // Fix warning with angular 4 - Critical dependency: the request of a dependency is an expression
      new webpack.ContextReplacementPlugin(/angular(\\|\/)core(\\|\/)@angular/, path.resolve(__dirname, '../src')),
      // Moves styles into a separate css file
      new ExtractTextPlugin({filename: 'css/[name].css'}),

      // Generates resulting html from a template
      new HtmlWebpackPlugin({
        template: './index.html',
        hash: true,
        chunksSortMode: 'dependency',
        minify: isDev ? false : htmlMinifierOptions
      }),

      // Pass global constants to the project
      new webpack.DefinePlugin({
        ENV_CONFIG: JSON.stringify({ENV, apiUrl: env.apiUrl})
      })
    ],
    devtool: isDev ? 'source-map' : false
  };

  if (!isDev) {
    config.plugins = config.plugins.concat([
      // Cleans build directory
      new CleanWebpackPlugin(['public'], {
        root: __dirname,
        verbose: true
      }),

      // Minifies js
      new UglifyJSPlugin()
    ]);
  }

  return config;
};
